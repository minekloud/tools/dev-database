# Dev Database

## Installation
Docker-compose: [Install Documentation](https://docs.docker.com/compose/install/)

OR for linux:

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## Start

```
docker-compose up
```

| Service | Host | Port | Login | Password |
|---|---|---|---|---|
| Postgres | localhost | 5432 | admin | password |
| pgAdmin | localhost | 5050 | admin@minekloud.com | password |

When you first login to pgadmin in your web browser, pg server is already added and ready to use

~~(note: we tried to use `pgpassfile` to automaticaly setup DB password but didn't work)~~ We did it ! 🥳🎉

## Remove
Remove the containers:
```
docker-compose down
```

If you want to also delete the volumes:
```
docker-compose down -v
```

## Config

Files in `scripts` folder are only executed when you **first** create the container and must be `.sql` or `.bash`